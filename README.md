# Hubble

A GitHub search engine!

# Demo

A demo is available at [bendyorke.gitlab.io/hubble](https://bendyorke.gitlab.io/hubble).

# Development

Hubble is built in ClojureScript, and compiled with boot. If you have a mac, it can be installed easily via homebrew:

```
$ brew install boot-clj
```

Otherwise you can find install methods [here](https://github.com/boot-clj/boot#install).

To start live-reload server, run:

```
$ boot dev
```

and open localhost:3001.

To view the production build (advanced google closure optimizations) run:

```
$ boot prod
```

and open `public/index.html`
