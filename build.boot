(set-env!
  :source-paths #{"src"}
  :asset-paths #{"assets"}
  :dependencies '[;; Clojure
                  [org.clojure/clojure "1.8.0"]
                  [org.clojure/clojurescript "1.8.51"]
                  [org.clojure/core.async "0.2.374"]
                  ;; Client
                  [reagent "0.6.0-alpha"]
                  [cljs-ajax "0.5.4"]
                  ;; Boot commands
                  [pandeiro/boot-http "0.7.3" :scope "test"]
                  [adzerk/boot-reload "0.4.5" :scope "test"]
                  [adzerk/boot-cljs "1.7.228-1" :scope "test"]
                  ;; boot-cljs-repl & deps
                  [adzerk/boot-cljs-repl "0.3.0" :scope "test"]
                  [com.cemerick/piggieback "0.2.1" :scope "test"]
                  [weasel "0.7.0" :scope "test"]
                  [org.clojure/tools.nrepl "0.2.12" :scope "test"]])

(require '[adzerk.boot-cljs :refer [cljs]]
         '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
         '[adzerk.boot-reload :refer [reload]]
         '[pandeiro.boot-http :refer [serve]])

(deftask c
  [s script bool "Start cljs repl?"]
  (repl :client true
        :eval (if script '(start-repl))))

(deftask dev []
  (comp
    (serve :reload true
           :port 3001
           :dir "public")
    (watch)
    (reload)
    (cljs-repl)
    (cljs :optimizations :none)
    (target :dir #{"public"})))

(deftask prod []
  (comp
    (cljs :optimizations :advanced)
    (target :dir #{"public"})))
