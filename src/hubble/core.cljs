(ns hubble.core
  (:require [reagent.core :as r]
            [hubble.api :as api]))

(def FOUND "✔")
(def ERROR "x")

(defonce app-state
  (r/atom
    {:org nil
     :found nil
     :repos [] ; [{:id # :name "" :stars #}...]
     :sort-key :name
     :sort-reverse? false}))

(defn sort-repos [key reverse? repos]
  (let [compare (if reverse? #(compare %2 %1) compare)]
    (sort-by key compare repos)))

(defn handle-repo [repo]
  {:id (get repo "id")
   :name (get repo "name")
   :description (get repo "description")
   :stars (get repo "stargazers_count")
   :forks (get repo "forks_count")
   :issues (get repo "open_issues_count")
   :size (get repo "size")})

(defn handle-new-repos [[repos headers]]
  (api/next-page headers
    {:handler handle-new-repos})
  (swap! app-state update :repos concat (map handle-repo repos)))

(defn handle-org-success []
  (swap! app-state assoc :found FOUND
                         :repos {})
  (api/repos (:org @app-state)
    {:handler handle-new-repos}))

(defn handle-org-fail []
  (swap! app-state assoc :found ERROR)
  (handle-new-repos []))

(defn handle-org-change [event]
  (swap! app-state merge {:org (.. event -target -value)
                          :found nil}))

(defn handle-org-submit [event]
  (.preventDefault event)
  (api/org (:org @app-state)
    {:handler handle-org-success
     :error-handler handle-org-fail}))

(defn input []
  [:form.search
   [:div (:found @app-state)]
   [:input {:type "text"
            :placeholder "Search a GitHub org..."
            :value (:org @app-state)
            :on-change handle-org-change}]
   [:button {:on-click handle-org-submit}
    "Search"]])

(defn field-header [key rev? field]
  (let [field-key     (-> field .toLowerCase keyword)
        current-sort  (= key field-key)
        click-handler (if current-sort
                         #(swap! app-state update :sort-reverse? not)
                         #(swap! app-state assoc :sort-key field-key))]
    ^{:key field-key}
    [:th {:on-click click-handler :style {:text-align :left}}
      field
      (when current-sort
        [:span {:style {:vertical-align :top}}
          (if rev? "⇂" "↾")])]))

(defn field-item [{:keys [id name description stars forks issues]}]
  ^{:key id}
  [:tr
   [:td
    [:b name]
    [:p description]]
   [:td stars]
   [:td forks]
   [:td issues]])

(defn repos []
  (fn []
    (let [{:keys [repos sort-key sort-reverse?]} @app-state]
      (when-not (empty? repos)
        [:div.table-container
         [:table
          [:thead
           [:tr
            (for [title ["Name" "Stars" "Forks" "Issues"]]
             (field-header sort-key sort-reverse? title))]]
          [:tbody
           (map field-item (sort-repos sort-key sort-reverse? repos))]]]))))

(defn main []
  [:div.container
    [:div.hero {:class (when (= FOUND (:found @app-state)) "hero-results")}
      [:img {:src "logo.png" :alt "Hubble"}]]
    [input]
    [repos]])


(r/render-component [main]
                    (.getElementById js/document "app"))
