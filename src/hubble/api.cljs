(ns hubble.api
  (:require [ajax.core :refer [GET transit-read-fn]]
            [clojure.string :as s]))

(enable-console-print!)

(def BASE "https://api.github.com")
(def AUTH "?access_token=f394e99f9d06f19497245a0be11cea1231083d17")

(defn parse-response-headers [headers]
 (reduce
   #(apply assoc %1 (s/split %2 #":" 2))
   {}
   (s/split headers "\r\n")))

(defn parse-response [xhrio]
  (let [body    ((transit-read-fn {}) xhrio)
        headers (-> xhrio .getAllResponseHeaders
                          parse-response-headers)]
    [body headers xhrio]))

(defn pages [headers]
  (let [link->map (fn [coll [val key]]
                    (assoc coll (keyword key) val))]
    (as-> headers _
          (get _ "Link" "")
          (s/split _ #"(?:; rel=|, )")
          (map #(s/replace %1 #"[<>\" ]" "") _)
          (partition 2 _)
          (reduce link->map {} _))))

(defn uri [uri?]
  (if-not (s/includes? uri? BASE)
    (str BASE uri? AUTH)
    uri?))

(defn fetch
  ([] (fetch "/"))
  ([path]
   (fetch path {:handler #(println (str %))
                :error-handler #(println (str "Error: " %))}))
  ([path opts]
   (GET (uri path)
        (assoc opts :response-format
          {:description "parsed"
           :read parse-response}))))

(defn org
  ([name]
   (fetch (str "/orgs/" name)))
  ([name opts]
   (fetch (str "/orgs/" name) opts)))

(defn repos
  ([org-name]
   (fetch (str "/orgs/" org-name "/repos")))
  ([org-name opts]
   (fetch (str "/orgs/" org-name "/repos") opts)))

(defn next-page [headers opts]
  (let [pages (pages headers)]
    (if-let [uri (:next pages)]
      (fetch uri opts))))
